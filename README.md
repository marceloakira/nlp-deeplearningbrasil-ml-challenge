# classification-challenge

Soluções para uma competição da turma: dados do Mercado Livre - Treinar um modelo de aprendizado de máquina para categorizar a publicações de produtos com base em seus títulos. https://www.kaggle.com/c/nlp-deeplearningbrasil-ml-challenge/leaderboard

Obs.: a visualização de Notebooks Jupyter pelo Gitlab não é boa. Sugestão: visualize o [notebook pelo nbviewer](https://nbviewer.jupyter.org/urls/gitlab.com/marceloakira/nlp-deeplearningbrasil-ml-challenge/raw/master/ClassificacaoDeProdutosDoMercadoLivre.ipynb)
